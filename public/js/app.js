
var myApp = angular.module('myApp',['ngRoute']);



myApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'html/login.html',
        controller: 'LoginController'
      }).
      when('/qr', {
        templateUrl: 'html/qr.html',
        controller: 'QRController'
      }).
      when('/events', {
        templateUrl: 'html/events.html',
        controller: 'EventsController'
      }).
      otherwise({
        redirectTo: '/login'
      });
  }]);

