
myApp.controller('QRController', function($scope, $interval) {
  $(document).ready(function() {
		$("#webcam").scriptcam({
			onError:onError,
			cornerRadius:0,
			onWebcamReady:onWebcamReady
		});
	});

	function onError(errorId,errorMsg) {
		alert(errorMsg);
	}			
	function changeCamera() {
		$.scriptcam.changeCamera($('#cameraNames').val());
	}
	function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
		$.each(cameraNames, function(index, text) {
			$('#cameraNames').append( $('<option></option>').val(index).html(text) )
		}); 
		$('#cameraNames').val(camera);

		var interval = $interval(function() {
			var text = $.scriptcam.getBarCode();
			if (text) {
				$('#decoded').text($.scriptcam.getBarCode());
				$interval.cancel(interval);
			};
			
		}, 500);
	}

	
})
.controller('LoginController', function($scope, $http, $location) {
	$scope.doLogin = function() {

		$http({
		  method: 'POST',
		  url: 'api/v1/login',
		  data: {
		  	email: $scope.email,
		  	password: $scope.password
		  }
		}).then(function successCallback(response) {console.log(response.data);
			if (response.data.success == true) {
				$location.path('events');
			} else {
				alert("Username or password is incorrect");
			}
		  }, function errorCallback(response) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		  });
	}
})
.controller('EventsController', function($scope, $http, $location) {
	
})
;

