<!DOCTYPE html>
<html ng-app="myApp">
    <head>
        <title>Laravel</title>

        <!-- <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css"> -->
        <% HTML::style('css/style.css') %>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
        <% HTML::script('js/scriptcam.js') %>
        <% HTML::script('js/lib/angular.js') %>
        <% HTML::script('js/lib/angular-route.js') %>
        <% HTML::script('js/app.js') %>
        <% HTML::script('js/controllers.js') %>        
        

    </head>
    <body ng-app="myApp">
        <header>
            <div class="header">Company</div>
        </header>        
        <div class="container main-content">
            <div ng-view></div>
        </div>        
    </body>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</html>
